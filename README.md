oneline laravel captcha
=======================

this repo is under refactoring

<h4>Check Codes First</h4>

<pre><code>
Route::get('/', function()
{
  return "&#60;img src='".url('captcha')."' &#62;";
});

Route::get('captcha',function() { Captcha::captcha(); } );
</code></pre>

=======================

<h3>Installation</h3>

Begin by installing this package through Composer. Edit your project's composer.json file to require gilak/captcha.

<pre><code>
"require-dev": {
  "gilak/captcha": "dev-master"
}
</code></pre>

do the 

<pre>
composer update --dev
</pre>

and just add this line below as new service provider item into the app/config/app.php file

<pre>
'Gilak\Captcha\CaptchaServiceProvider',
</pre>

=======================

<h3>Usage</h3>

first create a link for your captcha like bellow

<pre>
Route::get('captcha',function() { Captcha::captcha(); } );
</pre>

then call it in your view 

<pre>
&#60;img src='{{ url("captcha") }}' &#62;
</pre>

or 

<pre>
Route::get('captcha', 'AdminController@captcha');
</pre>

then call it in your view 

<pre>
&#60;img src='{{ action("AdminController@captcha") }}' &#62;
</pre>  

and lets come down to the validation 

simply use 

<pre>
Captcha::check()
</pre>
if its return true it means your captcha is passed ( wrongly )


=======================

<h3>Will Be Add</h3>

1- ajax refresh

2- soundify 
