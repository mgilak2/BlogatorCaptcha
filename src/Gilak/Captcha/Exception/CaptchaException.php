<?php namespace Gilak\Exception;

class CaptchaException extends \Exception
{

    protected $errors;

    function __construct($errors)
    {
        $this->errors = $errors;
    }

    function getErrors()
    {
        return $this->errors;
    }
}
