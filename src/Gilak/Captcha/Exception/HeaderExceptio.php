<?php namespace Gilak\Exception;

class HeaderException extends \Exception
{

    protected $errors;

    function __construct($errors)
    {
        $this->errors = $errors;
    }

    function getErrors()
    {
        return $this->errors;
    }
}
