<?php namespace Gilak\Captcha;

use Gilak\Captcha\Core\Core;
use Gilak\Captcha\Exception\CaptchaException;
use Gilak\Captcha\Exception\FormatException;
use Gilak\Captcha\Exception\HeaderException;
use Gilak\Captcha\Exception\ImageCreationException;
use Gilak\Captcha\Interfaces\iCrossUsage;
use Input;
use Session;

class Captcha extends Core implements iCrossUsage
{
    private $conf;

    public static function check()
    {
        if (strtolower(Input::get("captcha")) != strtolower(Session::get("captcha")))
            return true;
        else
            return false;
    }

    public static function captcha()
    {
        $captcha = new Captcha(
            new \Gilak\Captcha\Core\Image(),
            new \Gilak\Captcha\Core\Headers(),
            new \Gilak\Captcha\Core\Util()
        );
        $captcha->create();
    }
}
