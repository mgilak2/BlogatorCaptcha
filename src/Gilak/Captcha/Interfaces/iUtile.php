<?php namespace Gilak\Captcha\Interfaces;

interface iUtil
{
    public function randomText();
    public function setSession();
}