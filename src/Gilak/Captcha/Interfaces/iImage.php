<?php namespace Gilak\Captcha\Interfaces;

interface iImage
{

    public function createImage();

    public function intoPng();

    public function intoJpeg();

}