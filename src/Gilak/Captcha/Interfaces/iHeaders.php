<?php namespace Gilak\Captcha\Interfaces;

interface iHeaders
{
    public function setHeaders();
    public function setPngOrJpegHeader();
}