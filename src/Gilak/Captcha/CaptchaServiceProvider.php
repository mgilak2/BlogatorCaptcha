<?php namespace Gilak\Captcha;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class CaptchaServiceProvider extends ServiceProvider
{
    public function register()
    {
        // $this->app->bind( 'Captcha' , 'Gilak\Captcha\CaptchaService' );
    }

    public function boot()
    {
        $this->package("gilak/captcha");
        AliasLoader::getInstance()->alias('Captcha', 'Gilak\Captcha\Captcha');
    }
}
